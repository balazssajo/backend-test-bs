const {
  Application,
  HeartBeatController,
  HttpMethod,
  LoggerAccessor,
  LogLevel,
  createRouteBuilder,
  DatabaseProvider,
  CacheProvider
} = require("@greeneyesai/api-utils");
const path = require("path");

(async function main() {
  const logger = LoggerAccessor.setLogLevel(LogLevel.DEBUG).consoleLogger;

  try {
    const routeFactory = {
      create() {
        const apiBuilder = createRouteBuilder("/common", []);
        const routes = [
          apiBuilder(
            "/status",
            HttpMethod.GET,
            HeartBeatController,
            "heartBeat",
            []
          ),
        ];
        return [...routes];
      },
    };

    const Config = {
      DatabaseConfig: {
        host: process.env.DATABASE_HOST,
        port: Number(process.env.DATABASE_PORT),
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_DATABASE,
        ssl: false,
        modelsPath: path.resolve(__dirname, "./models"),
      },
      CacheConfig: {
        url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}/1`,
      },
    };

    const databaseProviderDefinition = {
      class: DatabaseProvider,
      config: Config.DatabaseConfig,
    };

    const cacheProviderDefinition = {
      class: CacheProvider,
      config: Config.CacheConfig,
    };

    const providers = [databaseProviderDefinition, cacheProviderDefinition];

    await new Application(8080)
      .setLoggerInterface(logger)
      .configureProviders(providers)
      .mountRoutes(routeFactory)
      .listen();
  } catch (e) {
    logger.error(e, () => process.exit(1));
  }
})();
